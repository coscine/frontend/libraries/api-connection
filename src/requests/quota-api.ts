const axios = require('axios');

import apiConnectionBasic from '../basic/api-connection-basic';

function getQuotaApiUrl() {
  return (
    'https://' + apiConnectionBasic.getHostName() + '/coscine/api/Coscine.Api.Quota/Quota/'
  );
}

export class QuotaApi {
  public static getResourceQuotas(
    projectId: string,
    resourceTypeId: string,
    thenHandler: any = apiConnectionBasic.defaultThenHandler,
    catchHandler: any = apiConnectionBasic.defaultOnCatch
  ) {
    apiConnectionBasic.setHeader();
    return axios
      .get(getQuotaApiUrl() + projectId + '/' + resourceTypeId + '/all')
      .then(thenHandler)
      .catch(catchHandler);
  }
  public static getResourceQuota(
    resourceId: string,
    thenHandler: any = apiConnectionBasic.defaultThenHandler,
    catchHandler: any = apiConnectionBasic.defaultOnCatch
  ) {
    apiConnectionBasic.setHeader();
    return axios
      .get(getQuotaApiUrl() + resourceId)
      .then(thenHandler)
      .catch(catchHandler);
  }
  public static setResourceQuota(
    resourceId: string,
    body: any,
    thenHandler: any = apiConnectionBasic.defaultThenHandler,
    catchHandler: any = apiConnectionBasic.defaultOnCatch,
  ) {
    apiConnectionBasic.setHeader();
    return axios
      .post(getQuotaApiUrl() + resourceId, body)
      .then(thenHandler)
      .catch(catchHandler);
  }
}
