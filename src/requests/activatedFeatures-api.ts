const axios = require('axios');

import apiConnectionBasic from '../basic/api-connection-basic';

function getActivatedFeaturesApiUrl() {
  return (
    'https://' +
    apiConnectionBasic.getHostName() +
    '/coscine/api/Coscine.Api.ActivatedFeatures/ActivatedFeatures/'
  );
}

export class ActivatedFeaturesApi {
  public static listAllFeatures(
    thenHandler: any = apiConnectionBasic.defaultThenHandler,
    catchHandler: any = apiConnectionBasic.defaultOnCatch
  ) {
    apiConnectionBasic.setHeader();
    return axios
      .get(getActivatedFeaturesApiUrl())
      .then(thenHandler)
      .catch(catchHandler);
  }

  public static listAllFeaturesOfProject(
    projectId: any,
    thenHandler: any = apiConnectionBasic.defaultThenHandler,
    catchHandler: any = apiConnectionBasic.defaultOnCatch
  ) {
    apiConnectionBasic.setHeader();
    return axios
      .get(getActivatedFeaturesApiUrl() + projectId)
      .then(thenHandler)
      .catch(catchHandler);
  }

  public static getActiveFeatures(
    projectId: any,
    thenHandler: any = apiConnectionBasic.defaultThenHandler,
    catchHandler: any = apiConnectionBasic.defaultOnCatch
  ) {
    apiConnectionBasic.setHeader();
    return axios
      .get(getActivatedFeaturesApiUrl() + projectId + '/activeFeatures')
      .then(thenHandler)
      .catch(catchHandler);
  }

  public static getInactiveFeatures(
    projectId: any,
    thenHandler: any = apiConnectionBasic.defaultThenHandler,
    catchHandler: any = apiConnectionBasic.defaultOnCatch
  ) {
    apiConnectionBasic.setHeader();
    return axios
      .get(getActivatedFeaturesApiUrl() + projectId + '/inactiveFeatures')
      .then(thenHandler)
      .catch(catchHandler);
  }

  public static activateFeature(
    projectId: any,
    featureId: any,
    thenHandler: any = apiConnectionBasic.defaultThenHandler,
    catchHandler: any = apiConnectionBasic.defaultOnCatch
  ) {
    apiConnectionBasic.setHeader();
    return axios
      .get(getActivatedFeaturesApiUrl() + projectId + '/activateFeature/' + featureId)
      .then(thenHandler)
      .catch(catchHandler);
  }

  public static deactivateFeature(
    projectId: any,
    featureId: any,
    thenHandler: any = apiConnectionBasic.defaultThenHandler,
    catchHandler: any = apiConnectionBasic.defaultOnCatch
  ) {
    apiConnectionBasic.setHeader();
    return axios
      .get(getActivatedFeaturesApiUrl() + projectId + '/deactivateFeature/' + featureId)
      .then(thenHandler)
      .catch(catchHandler);
  }
}
