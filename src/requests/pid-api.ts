const axios = require('axios');

import apiConnectionBasic from '../basic/api-connection-basic';

function getPIDApiUrl() {
  return (
    'https://' + apiConnectionBasic.getHostName() + '/coscine/api/Coscine.Api.Pid/Pid/'
  );
}

export class PIDApi {
  public static sendMailToOwner(
    body: any,
    thenHandler: any = apiConnectionBasic.defaultThenHandler,
    catchHandler: any = apiConnectionBasic.defaultOnCatch
  ) {
    return axios
      .post(getPIDApiUrl() + 'sendMailToOwner', body)
      .then(thenHandler)
      .catch(catchHandler);
  }
}
