const axios = require('axios');

import apiConnectionBasic from '../basic/api-connection-basic';
import { profile } from 'console';

function getMetadataApiUrl() {
  return (
    'https://' + apiConnectionBasic.getHostName() + '/coscine/api/Coscine.Api.Metadata/Metadata/'
  );
}

export class MetadataApi {
  public static getProfiles(
    thenHandler: any = apiConnectionBasic.defaultThenHandler,
    catchHandler: any = apiConnectionBasic.defaultOnCatch
  ) {
    apiConnectionBasic.setHeader();
    return axios
      .get(getMetadataApiUrl() + 'profiles/')
      .then(thenHandler)
      .catch(catchHandler);
  }
  public static getProfile(
    profile: string,
    thenHandler: any = apiConnectionBasic.defaultThenHandler,
    catchHandler: any = apiConnectionBasic.defaultOnCatch
  ) {
    apiConnectionBasic.setHeader();
    return axios
      .get(getMetadataApiUrl() + 'profiles/' + encodeURIComponent(profile))
      .then(thenHandler)
      .catch(catchHandler);
  }
  public static getProfileComplete(
    profile: string,
    resourceId: string,
    thenHandler: any = apiConnectionBasic.defaultThenHandler,
    catchHandler: any = apiConnectionBasic.defaultOnCatch
  ) {
    apiConnectionBasic.setHeader();
    return axios
      .get(getMetadataApiUrl() + 'profiles/' + encodeURIComponent(profile) + '/' + resourceId)
      .then(thenHandler)
      .catch(catchHandler);
  }
  public static getVocabularies(
    thenHandler: any = apiConnectionBasic.defaultThenHandler,
    catchHandler: any = apiConnectionBasic.defaultOnCatch
  ) {
    apiConnectionBasic.setHeader();
    return axios
      .get(getMetadataApiUrl() + 'vocabularies/')
      .then(thenHandler)
      .catch(catchHandler);
  }
  public static getVocabulary(
    path: string,
    thenHandler: any = apiConnectionBasic.defaultThenHandler,
    catchHandler: any = apiConnectionBasic.defaultOnCatch
  ) {
    apiConnectionBasic.setHeader();
    return axios
      .get(getMetadataApiUrl() + 'vocabularies/' + encodeURIComponent(path))
      .then(thenHandler)
      .catch(catchHandler);
  }

  public static getClassInstances(
    projectId: string,
    className: string,
    thenHandler: any = apiConnectionBasic.defaultThenHandler,
    catchHandler: any = apiConnectionBasic.defaultOnCatch
  ) {
    apiConnectionBasic.setHeader();
    return axios
      .get(getMetadataApiUrl() + 'instances/' + projectId + '/' + encodeURIComponent(className))
      .then(thenHandler)
      .catch(catchHandler);
  }
}
