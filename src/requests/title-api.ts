const axios = require('axios');

import apiConnectionBasic from '../basic/api-connection-basic';

function getTitleApiUrl() {
  return (
    'https://' + apiConnectionBasic.getHostName() + '/coscine/api/Coscine.Api.User/Title/'
  );
}

export class TitleApi {
  public static getTitles(
    thenHandler: any = apiConnectionBasic.defaultThenHandler,
    catchHandler: any = apiConnectionBasic.defaultOnCatch
  ) {
    apiConnectionBasic.setHeader();
    return axios
      .get(getTitleApiUrl())
      .then(thenHandler)
      .catch(catchHandler);
  }
}
