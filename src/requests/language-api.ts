const axios = require('axios');

import apiConnectionBasic from '../basic/api-connection-basic';

function getLanguageApiUrl() {
  return (
    'https://' + apiConnectionBasic.getHostName() + '/coscine/api/Coscine.Api.User/Language/'
  );
}

export class LanguageApi {
  public static getLanguages(
    thenHandler: any = apiConnectionBasic.defaultThenHandler,
    catchHandler: any = apiConnectionBasic.defaultOnCatch
  ) {
    apiConnectionBasic.setHeader();
    return axios
      .get(getLanguageApiUrl())
      .then(thenHandler)
      .catch(catchHandler);
  }
}
