const axios = require('axios');

import apiConnectionBasic from '../basic/api-connection-basic';

function getLicenseApiUrl() {
  return (
    'https://' + apiConnectionBasic.getHostName() + '/coscine/api/Coscine.Api.Project/License/'
  );
}

export class LicenseApi {
  public static getLicenses(
    thenHandler: any = apiConnectionBasic.defaultThenHandler,
    catchHandler: any = apiConnectionBasic.defaultOnCatch
  ) {
    apiConnectionBasic.setHeader();
    return axios
      .get(getLicenseApiUrl())
      .then(thenHandler)
      .catch(catchHandler);
  }
}
