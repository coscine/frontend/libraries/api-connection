const axios = require('axios');

import apiConnectionBasic from '../basic/api-connection-basic';

function getVisibilityApiUrl() {
  return (
    'https://' + apiConnectionBasic.getHostName() + '/coscine/api/Coscine.Api.Project/Visibility/'
  );
}

export class VisibilityApi {
  public static getVisibilities(
    thenHandler: any = apiConnectionBasic.defaultThenHandler,
    catchHandler: any = apiConnectionBasic.defaultOnCatch
  ) {
    apiConnectionBasic.setHeader();
    return axios
      .get(getVisibilityApiUrl())
      .then(thenHandler)
      .catch(catchHandler);
  }
}
