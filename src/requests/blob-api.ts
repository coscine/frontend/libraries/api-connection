const axios = require('axios');
import fileSaver from 'file-saver';

import apiConnectionBasic from '../basic/api-connection-basic';

function getBlobApiUrl() {
  return (
    'https://' + apiConnectionBasic.getHostName() + '/coscine/api/Coscine.Api.Blob/Blob/'
  );
}

export class BlobApi {
  public static downloadObject(
    resourceId: any,
    path: string,
    name: string,
    catchHandler: any = apiConnectionBasic.defaultOnCatch
  ) {
    return BlobApi.getDownloadObject(
      resourceId,
      path,
      (response: any) => {
        // Let the user save the file.
        // The data is cached until the response is completed!
        fileSaver.saveAs(response.data, name);
        window.location.hash = window.location.hash.substring(0, window.location.hash.lastIndexOf('/'));
      },
      catchHandler
    );
  }

  public static getDownloadObject(
    resourceId: any,
    path: string,
    thenHandler: any = apiConnectionBasic.defaultThenHandler,
    catchHandler: any = apiConnectionBasic.defaultOnCatch
  ) {
    apiConnectionBasic.setHeader();
    return axios
      .get(getBlobApiUrl() + resourceId + '/' + apiConnectionBasic.preparePath(path), {
        responseType: 'blob'
      })
      .then(thenHandler)
      .catch(catchHandler);
  }

  public static deleteObject(
    resourceId: any,
    path: any,
    thenHandler: any = apiConnectionBasic.defaultThenHandler,
    catchHandler: any = apiConnectionBasic.defaultOnCatch
  ) {
    apiConnectionBasic.setHeader();
    return axios
      .delete(getBlobApiUrl() + resourceId + '/' + apiConnectionBasic.preparePath(path))
      .then(thenHandler)
      .catch(catchHandler);
  }

  public static uploadObject(
    resourceId: any,
    path: any,
    data: any,
    thenHandler: any = apiConnectionBasic.defaultThenHandler,
    catchHandler: any = apiConnectionBasic.defaultOnCatch
  ) {
    apiConnectionBasic.setHeader();
    return axios({
      method: 'PUT',
      url: getBlobApiUrl() + resourceId + '/' + apiConnectionBasic.preparePath(path),
      headers: {
        'Content-Type': 'multipart/form-data'
      },
      data
    })
      .then(thenHandler)
      .catch(catchHandler);
  }

  public static isResourceValid(
    body: any,
    thenHandler: any = apiConnectionBasic.defaultThenHandler,
    catchHandler: any = apiConnectionBasic.defaultOnCatch
  ) {
    apiConnectionBasic.setHeader();
    return axios
      .post(getBlobApiUrl() + 'validate', body)
      .then(thenHandler)
      .catch(catchHandler);
  }

  public static getQuota(
    resourceId: any,
    thenHandler: any = apiConnectionBasic.defaultThenHandler,
    catchHandler: any = apiConnectionBasic.defaultOnCatch
  ) {
    apiConnectionBasic.setHeader();
    return axios
      .get(getBlobApiUrl() + resourceId + '/quota')
      .then(thenHandler)
      .catch(catchHandler);
  }
}
