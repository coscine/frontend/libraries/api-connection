const axios = require('axios');

import apiConnectionBasic from '../basic/api-connection-basic';

function getContactChangeApiUrl() {
  return (
    'https://' + apiConnectionBasic.getHostName() + '/coscine/api/Coscine.Api.User/ContactChange/'
  );
}

export class ContactChangeApi {
  public static getStatus(
    thenHandler: any = apiConnectionBasic.defaultThenHandler,
    catchHandler: any = apiConnectionBasic.defaultOnCatch
  ) {
    apiConnectionBasic.setHeader();
    return axios
      .get(getContactChangeApiUrl() + 'status')
      .then(thenHandler)
      .catch(catchHandler);
  }
  public static confirm(
    token: string,
    thenHandler: any = apiConnectionBasic.defaultThenHandler,
    catchHandler: any = apiConnectionBasic.defaultOnCatch
  ) {
    return axios
      .get(getContactChangeApiUrl() + 'confirm/' + token)
      .then(thenHandler)
      .catch(catchHandler);
  }
  public static emailChange(
    body: any,
    thenHandler: any = apiConnectionBasic.defaultThenHandler,
    catchHandler: any = apiConnectionBasic.defaultOnCatch
  ) {
    apiConnectionBasic.setHeader();
    const config = { headers: {'Content-Type': 'application/json'} };
    return axios
      .post(getContactChangeApiUrl() + 'emailchange', body, config)
      .then(thenHandler)
      .catch(catchHandler);
  }
}
