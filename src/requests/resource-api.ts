const axios = require('axios');

import apiConnectionBasic from '../basic/api-connection-basic';

function getResourceApiUrl() {
  return (
    'https://' + apiConnectionBasic.getHostName() + '/coscine/api/Coscine.Api.Resources/Resource/'
  );
}

export class ResourceApi {
  public static getResourceInformation(
    resourceId: string,
    thenHandler: any = apiConnectionBasic.defaultThenHandler,
    catchHandler: any = apiConnectionBasic.defaultOnCatch
  ) {
    apiConnectionBasic.setHeader();
    return axios
      .get(getResourceApiUrl() + resourceId)
      .then(thenHandler)
      .catch(catchHandler);
  }
  public static storeResource(
    parentId: string,
    body: any,
    thenHandler: any = apiConnectionBasic.defaultThenHandler,
    catchHandler: any = apiConnectionBasic.defaultOnCatch
  ) {
    apiConnectionBasic.setHeader();
    return axios
      .post(getResourceApiUrl() + 'Project/' + parentId, body)
      .then(thenHandler)
      .catch(catchHandler);
  }
  public static deleteResource(
    resourceId: string,
    thenHandler: any = apiConnectionBasic.defaultThenHandler,
    catchHandler: any = apiConnectionBasic.defaultOnCatch
  ) {
    apiConnectionBasic.setHeader();
    return axios
      .delete(getResourceApiUrl() + resourceId)
      .then(thenHandler)
      .catch(catchHandler);
  }
  public static updateResource(
    resourceId: string,
    body: any,
    thenHandler: any = apiConnectionBasic.defaultThenHandler,
    catchHandler: any = apiConnectionBasic.defaultOnCatch
  ) {
    apiConnectionBasic.setHeader();
    return axios
      .post(getResourceApiUrl() + resourceId, body)
      .then(thenHandler)
      .catch(catchHandler);
  }
  public static isUserResourceCreator(
    resourceId: string,
    thenHandler: any = apiConnectionBasic.defaultThenHandler,
    catchHandler: any = apiConnectionBasic.defaultOnCatch
  ) {
    apiConnectionBasic.setHeader();
    return axios
      .get(getResourceApiUrl() + resourceId + '/isCreator')
      .then(thenHandler)
      .catch(catchHandler);
  }
  public static setResourceReadonly(
    resourceId: string,
    isResourceReadOnly: boolean,
    thenHandler: any = apiConnectionBasic.defaultThenHandler,
    catchHandler: any = apiConnectionBasic.defaultOnCatch
  ) {
    apiConnectionBasic.setHeader();
    return axios
      .post(getResourceApiUrl() + resourceId + '/setReadonly?status='+ isResourceReadOnly)
      .then(thenHandler)
      .catch(catchHandler);
  }
}
