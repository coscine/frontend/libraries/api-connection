const axios = require('axios');

import apiConnectionBasic from '../basic/api-connection-basic';

function getTreeApiUrl() {
  return (
    'https://' + apiConnectionBasic.getHostName() + '/coscine/api/Coscine.Api.Tree/Tree/'
  );
}

export class TreeApi {
  public static getMetadata(
    resourceId: string,
    path: string,
    thenHandler: any = apiConnectionBasic.defaultThenHandler,
    catchHandler: any = apiConnectionBasic.defaultOnCatch
  ) {
    apiConnectionBasic.setHeader();
    return axios
      .get(getTreeApiUrl() + resourceId + '/' + apiConnectionBasic.preparePath(path))
      .then(thenHandler)
      .catch(catchHandler);
  }

  public static storeMetadataForFile(
    resourceId: string,
    path: string,
    body: { [index: string]: Array<{ value: any }> },
    thenHandler: any = apiConnectionBasic.defaultThenHandler,
    catchHandler: any = apiConnectionBasic.defaultOnCatch
  ) {
    apiConnectionBasic.setHeader();

    // remove empty nodes
    for (let node in body) {
      let property = body[node];
      if (
        property.length === 0 ||
        property[0].value === undefined ||
        property[0].value === null ||
        property[0].value.trim() === ''
      ) {
        delete body[node];
      }
    }
    
    return axios
      .put(getTreeApiUrl() + resourceId + '/' + apiConnectionBasic.preparePath(path), body)
      .then(thenHandler)
      .catch(catchHandler);
  }
}
