const axios = require('axios');

import apiConnectionBasic from '../basic/api-connection-basic';

function getTokenApiUrl() {
  return 'https://' + apiConnectionBasic.getHostName() + '/coscine/api/Coscine.Api.Token/Token/';
}

export class TokenApi {
  public static GetUserTokens(
    thenHandler: any = apiConnectionBasic.defaultThenHandler,
    catchHandler: any = apiConnectionBasic.defaultOnCatch
  ) {
    apiConnectionBasic.setHeader();
    return axios
      .get(getTokenApiUrl())
      .then(thenHandler)
      .catch(catchHandler);
  }
  public static AddToken(
    body: any,
    thenHandler: any = apiConnectionBasic.defaultThenHandler,
    catchHandler: any = apiConnectionBasic.defaultOnCatch
  ) {
    apiConnectionBasic.setHeader();
    return axios
      .put(getTokenApiUrl(), body)
      .then(thenHandler)
      .catch(catchHandler);
  }
  public static RevokeToken(
    tokenId: string,
    thenHandler: any = apiConnectionBasic.defaultThenHandler,
    catchHandler: any = apiConnectionBasic.defaultOnCatch
  ) {
    apiConnectionBasic.setHeader();
    return axios
      .delete(getTokenApiUrl() + tokenId)
      .then(thenHandler)
      .catch(catchHandler);
  }
  public static GetUserToken(
    tokenId: string,
    thenHandler: any = apiConnectionBasic.defaultThenHandler,
    catchHandler: any = apiConnectionBasic.defaultOnCatch,
  ) {
    apiConnectionBasic.setHeader();
    return axios
      .get(getTokenApiUrl() + tokenId)
      .then(thenHandler)
      .catch(catchHandler);
  }
}
