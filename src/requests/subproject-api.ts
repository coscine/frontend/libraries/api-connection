const axios = require('axios');

import apiConnectionBasic from '../basic/api-connection-basic';

function getSubProjectApiUrl() {
  return (
    'https://' + apiConnectionBasic.getHostName() + '/coscine/api/Coscine.Api.Project/SubProject/'
  );
}

export class SubProjectApi {
  public static getSubProjects(
    parentId: string,
    thenHandler: any = apiConnectionBasic.defaultThenHandler,
    catchHandler: any = apiConnectionBasic.defaultOnCatch
  ) {
    apiConnectionBasic.setHeader();
    return axios
      .get(getSubProjectApiUrl() + parentId + '/')
      .then(thenHandler)
      .catch(catchHandler);
  }
  public static getAccessibleParent(
    childId: string,
    thenHandler: any = apiConnectionBasic.defaultThenHandler,
    catchHandler: any = apiConnectionBasic.defaultOnCatch
  ) {
    apiConnectionBasic.setHeader();
    return axios
      .get(getSubProjectApiUrl() + childId + '/accessibleParent')
      .then(thenHandler)
      .catch(catchHandler);
  }
}
