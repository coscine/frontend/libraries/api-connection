const axios = require('axios');

import apiConnectionBasic from '../basic/api-connection-basic';

function getUserApiUrl() {
  return 'https://' + apiConnectionBasic.getHostName() + '/coscine/api/Coscine.Api.User/User/';
}

export class UserApi {
  public static queryUsers(
    query: string,
    parentId: string,
    thenHandler: any = apiConnectionBasic.defaultThenHandler,
    catchHandler: any = apiConnectionBasic.defaultOnCatch
  ) {
    apiConnectionBasic.setHeader();
    return axios
      .get(getUserApiUrl() + 'query/' + query + '/project/' + parentId + '/')
      .then(thenHandler)
      .catch(catchHandler);
  }

  public static getUser(
    thenHandler: any = apiConnectionBasic.defaultThenHandler,
    catchHandler: any = apiConnectionBasic.defaultOnCatch
  ) {
    apiConnectionBasic.setHeader();
    return axios
      .get(getUserApiUrl() + 'user')
      .then(thenHandler)
      .catch(catchHandler);
  }
  public static updateUser(
    body: any,
    thenHandler: any = apiConnectionBasic.defaultThenHandler,
    catchHandler: any = apiConnectionBasic.defaultOnCatch
  ) {
    apiConnectionBasic.setHeader();
    return axios
      .post(getUserApiUrl() + 'user', body)
      .then(thenHandler)
      .catch(catchHandler);
  }
  public static setAndReturnMergeToken(
    provider: string,
    thenHandler: any = apiConnectionBasic.defaultThenHandler,
    catchHandler: any = apiConnectionBasic.defaultOnCatch
  ) {
    apiConnectionBasic.setHeader();
    return axios
      .get(getUserApiUrl() + 'mergeToken/' + provider)
      .then(thenHandler)
      .catch(catchHandler);
  }
}
