const axios = require('axios');

import apiConnectionBasic from '../basic/api-connection-basic';

function getProjectApiUrl() {
  return (
    'https://' + apiConnectionBasic.getHostName() + '/coscine/api/Coscine.Api.Project/Project/'
  );
}

export class ProjectApi {
  public static getProjectInformation(
    projectId: string,
    thenHandler: any = apiConnectionBasic.defaultThenHandler,
    catchHandler: any = apiConnectionBasic.defaultOnCatch
  ) {
    apiConnectionBasic.setHeader();
    return axios
      .get(getProjectApiUrl() + projectId)
      .then(thenHandler)
      .catch(catchHandler);
  }
  public static storeProject(
    body: any,
    thenHandler: any = apiConnectionBasic.defaultThenHandler,
    catchHandler: any = apiConnectionBasic.defaultOnCatch
  ) {
    apiConnectionBasic.setHeader();
    return axios
      .post(getProjectApiUrl(), body)
      .then(thenHandler)
      .catch(catchHandler);
  }
  public static updateProject(
    projectId: string,
    body: any,
    thenHandler: any = apiConnectionBasic.defaultThenHandler,
    catchHandler: any = apiConnectionBasic.defaultOnCatch
  ) {
    apiConnectionBasic.setHeader();
    return axios
      .post(getProjectApiUrl() + projectId, body)
      .then(thenHandler)
      .catch(catchHandler);
  }
  public static getQuotas(
    projectId: string,
    thenHandler: any = apiConnectionBasic.defaultThenHandler,
    catchHandler: any = apiConnectionBasic.defaultOnCatch,
  ) {
    apiConnectionBasic.setHeader();
    return axios
      .get(getProjectApiUrl() + projectId + '/quota/-/all')
      .then(thenHandler)
      .catch(catchHandler);
  }
  public static getQuota(
    projectId: string,
    resourceTypeId: any,
    thenHandler: any = apiConnectionBasic.defaultThenHandler,
    catchHandler: any = apiConnectionBasic.defaultOnCatch
  ) {
    apiConnectionBasic.setHeader();
    return axios
      .get(getProjectApiUrl() + projectId + '/quota/' + resourceTypeId)
      .then(thenHandler)
      .catch(catchHandler);
  }
  public static getMaxiumQuota(
    projectId: string,
    resourceTypeId: any,
    thenHandler: any = apiConnectionBasic.defaultThenHandler,
    catchHandler: any = apiConnectionBasic.defaultOnCatch
  ) {
    apiConnectionBasic.setHeader();
    return axios
      .get(getProjectApiUrl() + projectId + '/quota/' + resourceTypeId + '/max')
      .then(thenHandler)
      .catch(catchHandler);
  }
  public static setQuota(
    projectId: string,
    resourceTypeId: string,
    body: any,
    thenHandler: any = apiConnectionBasic.defaultThenHandler,
    catchHandler: any = apiConnectionBasic.defaultOnCatch
  ) {
    apiConnectionBasic.setHeader();
    return axios
      .post(getProjectApiUrl() + projectId + '/quota/' + resourceTypeId, body)
      .then(thenHandler)
      .catch(catchHandler);
  }
  public static getResources(
    projectId: string,
    thenHandler: any = apiConnectionBasic.defaultThenHandler,
    catchHandler: any = apiConnectionBasic.defaultOnCatch,
    analyticsLogFlag: any = ""
  ) {
    apiConnectionBasic.setHeader();
    return axios
      .get(getProjectApiUrl() + projectId + '/resources' + analyticsLogFlag)
      .then(thenHandler)
      .catch(catchHandler);
  }
  public static getResourcesWithoutAnalyticsLog(
    projectId: string,
    thenHandler: any = apiConnectionBasic.defaultThenHandler,
    catchHandler: any = apiConnectionBasic.defaultOnCatch
  ) {
    ProjectApi.getResources(projectId, thenHandler, catchHandler, '?noanalyticslog=true');
  }
  public static deleteProject(
    projectId: string,
    thenHandler: any = apiConnectionBasic.defaultThenHandler,
    catchHandler: any = apiConnectionBasic.defaultOnCatch
  ) {
    apiConnectionBasic.setHeader();
    return axios
      .delete(getProjectApiUrl() + projectId)
      .then(thenHandler)
      .catch(catchHandler);
  }
  public static getProjects(
    thenHandler: any = apiConnectionBasic.defaultThenHandler,
    catchHandler: any = apiConnectionBasic.defaultOnCatch,
    analyticsLogFlag: any = ""
  ) {
    apiConnectionBasic.setHeader();
    return axios
      .get(getProjectApiUrl() + analyticsLogFlag)
      .then(thenHandler)
      .catch(catchHandler);
  }
  public static getProjectsWithoutAnalyticsLog(
    thenHandler: any = apiConnectionBasic.defaultThenHandler,
    catchHandler: any = apiConnectionBasic.defaultOnCatch
  ) {
    ProjectApi.getProjects(thenHandler, catchHandler, '?noanalyticslog=true');
  }
  public static getTopLevelProjects(
    thenHandler: any = apiConnectionBasic.defaultThenHandler,
    catchHandler: any = apiConnectionBasic.defaultOnCatch,
    analyticsLogFlag: any = ""
  ) {
    apiConnectionBasic.setHeader();
    return axios
      .get(getProjectApiUrl() + '-/topLevel' + analyticsLogFlag)
      .then(thenHandler)
      .catch(catchHandler);
  }
  public static getTopLevelProjectsWithoutAnalyticsLog(
    thenHandler: any = apiConnectionBasic.defaultThenHandler,
    catchHandler: any = apiConnectionBasic.defaultOnCatch
  ) {
    ProjectApi.getTopLevelProjects(thenHandler, catchHandler, '?noanalyticslog=true');
  }
  public static getInvitationsList(
    projectId: string,
    thenHandler: any = apiConnectionBasic.defaultThenHandler,
    catchHandler: any = apiConnectionBasic.defaultOnCatch
  ) {
    apiConnectionBasic.setHeader();
    return axios
      .get(getProjectApiUrl() + 'invitation/list/' + projectId)
      .then(thenHandler)
      .catch(catchHandler);
  }
  public static storeInvitation(
    body: any,
    thenHandler: any = apiConnectionBasic.defaultThenHandler,
    catchHandler: any = apiConnectionBasic.defaultOnCatch
  ) {
    apiConnectionBasic.setHeader();
    return axios
      .post(getProjectApiUrl() + 'invitation', body)
      .then(thenHandler)
      .catch(catchHandler);
  }
  public static deleteInvitation(
    invitationId: string,
    thenHandler: any = apiConnectionBasic.defaultThenHandler,
    catchHandler: any = apiConnectionBasic.defaultOnCatch
  ) {
    apiConnectionBasic.setHeader();
    return axios
      .delete(getProjectApiUrl() + 'invitation/' + invitationId)
      .then(thenHandler)
      .catch(catchHandler);
  }
  public static resolveInvitation(
    token: string,
    thenHandler: any = apiConnectionBasic.defaultThenHandler,
    catchHandler: any = apiConnectionBasic.defaultOnCatch
  ) {
    apiConnectionBasic.setHeader();
    return axios
      .get(getProjectApiUrl() + 'invitation/resolve/' + token)
      .then(thenHandler)
      .catch(catchHandler);
  }
}
