const axios = require('axios');

import apiConnectionBasic from '../basic/api-connection-basic';

function getNoticeApiUrl() {
  return (
    'https://' + apiConnectionBasic.getHostName() + '/coscine/api/Coscine.Api.Notices/Notice/'
  );
}

export class NoticeApi {
  public static getNotices(
    documentSlug: string,
    language: string,
    thenHandler: any = apiConnectionBasic.defaultThenHandler,
    catchHandler: any = apiConnectionBasic.defaultOnCatch
  ) {
    return axios
      .get(getNoticeApiUrl() + encodeURIComponent(documentSlug) + '?language=' + language)
      .then(thenHandler)
      .catch(catchHandler);
  }
  public static GetMaintenance(
    thenHandler: any = apiConnectionBasic.defaultThenHandler,
    catchHandler: any = apiConnectionBasic.defaultOnCatch
  ) {
    return axios
      .get(getNoticeApiUrl() + encodeURIComponent('getMaintenance'))
      .then(thenHandler)
      .catch(catchHandler);
  }
}
