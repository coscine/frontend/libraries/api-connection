const axios = require('axios');

import apiConnectionBasic from '../basic/api-connection-basic';

function getProjectRoleApiUrl() {
  return (
    'https://' + apiConnectionBasic.getHostName() + '/coscine/api/Coscine.Api.Project/ProjectRole/'
  );
}

export class ProjectRoleApi {
  public static getProjectRoles(
    parentId: string,
    thenHandler: any = apiConnectionBasic.defaultThenHandler,
    catchHandler: any = apiConnectionBasic.defaultOnCatch,
    analyticsLogFlag: any = ""
  ) {
    apiConnectionBasic.setHeader();
    return axios
      .get(getProjectRoleApiUrl() + parentId + '/' + analyticsLogFlag)
      .then(thenHandler)
      .catch(catchHandler);
  }
  public static getProjectRolesWithoutAnalyticsLog(
    parentId: string,
    thenHandler: any = apiConnectionBasic.defaultThenHandler,
    catchHandler: any = apiConnectionBasic.defaultOnCatch
  ) {
    ProjectRoleApi.getProjectRoles(parentId, thenHandler, catchHandler, '?noanalyticslog=true')
  }
  public static getUserRoles(
    projectId: string,
    thenHandler: any = apiConnectionBasic.defaultThenHandler,
    catchHandler: any = apiConnectionBasic.defaultOnCatch
  ) {
    apiConnectionBasic.setHeader();
    return axios
      .get(getProjectRoleApiUrl() + 'project/' + projectId + '/')
      .then(thenHandler)
      .catch(catchHandler);
  }
  public static setProjectRole(
    body: any,
    thenHandler: any = apiConnectionBasic.defaultThenHandler,
    catchHandler: any = apiConnectionBasic.defaultOnCatch
  ) {
    apiConnectionBasic.setHeader();
    return axios
      .post(getProjectRoleApiUrl(), body)
      .then(thenHandler)
      .catch(catchHandler);
  }
  public static deleteUser(
    parentId: any,
    userId: any,
    roleId: any,
    thenHandler: any = apiConnectionBasic.defaultThenHandler,
    catchHandler: any = apiConnectionBasic.defaultOnCatch
  ) {
    apiConnectionBasic.setHeader();
    return axios
      .delete(
        getProjectRoleApiUrl() + 'project/' + parentId + '/user/' + userId + '/role/' + roleId
      )
      .then(thenHandler)
      .catch(catchHandler);
  }
  public static leaveProject(
    projectId: any,
    thenHandler: any = apiConnectionBasic.defaultThenHandler,
    catchHandler: any = apiConnectionBasic.defaultOnCatch
  ) {
    apiConnectionBasic.setHeader();
    return axios
      .delete(
        getProjectRoleApiUrl() + 'project/' + projectId + '/user'
      )
      .then(thenHandler)
      .catch(catchHandler);
  }
}
