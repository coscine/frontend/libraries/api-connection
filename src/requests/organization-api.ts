const axios = require('axios');

import apiConnectionBasic from '../basic/api-connection-basic';

function getOrganizationApiUrl() {
  return (
    'https://' + apiConnectionBasic.getHostName() + '/coscine/api/Coscine.Api.Organization/Organization/'
  );
}

export class OrganizationApi {
  public static getOrganizations(
    thenHandler: any = apiConnectionBasic.defaultThenHandler,
    catchHandler: any = apiConnectionBasic.defaultOnCatch
  ) {
    apiConnectionBasic.setHeader();
    return axios
      .get(getOrganizationApiUrl())
      .then(thenHandler)
      .catch(catchHandler);
  }
  public static getOrganizationsWhereUserIsMember(
    thenHandler: any = apiConnectionBasic.defaultThenHandler,
    catchHandler: any = apiConnectionBasic.defaultOnCatch
  ) {
    apiConnectionBasic.setHeader();
    return axios
      .get(getOrganizationApiUrl() + '?member=1')
      .then(thenHandler)
      .catch(catchHandler);
  }
  public static getOrganizationsWithFilter(
    searchterm: string,
    thenHandler: any = apiConnectionBasic.defaultThenHandler,
    catchHandler: any = apiConnectionBasic.defaultOnCatch
  ) {
    apiConnectionBasic.setHeader();
    return axios
      .get(getOrganizationApiUrl() + '?filter=' + encodeURIComponent(searchterm))
      .then(thenHandler)
      .catch(catchHandler);
  }
  public static getOrganization(
    url: string,
    thenHandler: any = apiConnectionBasic.defaultThenHandler,
    catchHandler: any = apiConnectionBasic.defaultOnCatch
  ) {
    apiConnectionBasic.setHeader();
    return axios
      .get(getOrganizationApiUrl() + encodeURIComponent(url))
      .then(thenHandler)
      .catch(catchHandler);
  }
  public static getROR(
    searchterm: string,
    thenHandler: any = apiConnectionBasic.defaultThenHandler,
    catchHandler: any = apiConnectionBasic.defaultOnCatch
  ) {
    apiConnectionBasic.setHeader();
    return axios
      .get(getOrganizationApiUrl() + '-/ror?filter=' + encodeURIComponent(searchterm))
      .then(thenHandler)
      .catch(catchHandler);
  }
  public static isRWTHMember(
    thenHandler: any = apiConnectionBasic.defaultThenHandler,
    catchHandler: any = apiConnectionBasic.defaultOnCatch
  ) {
    apiConnectionBasic.setHeader();
    return axios
      .get(getOrganizationApiUrl() + '-/isMember/' + encodeURIComponent('https://ror.org/04xfq0f34'))
      .then(thenHandler)
      .catch(catchHandler);
  }
  public static isMember(
    organizationUrl: string,
    thenHandler: any = apiConnectionBasic.defaultThenHandler,
    catchHandler: any = apiConnectionBasic.defaultOnCatch
  ) {
    apiConnectionBasic.setHeader();
    return axios
      .get(getOrganizationApiUrl() + '-/isMember/' + encodeURIComponent(organizationUrl))
      .then(thenHandler)
      .catch(catchHandler);
  }
  public static getMemberships(
    thenHandler: any = apiConnectionBasic.defaultThenHandler,
    catchHandler: any = apiConnectionBasic.defaultOnCatch
  ) {
    apiConnectionBasic.setHeader();
    return axios
      .get(getOrganizationApiUrl() + '-/isMember')
      .then(thenHandler)
      .catch(catchHandler);
  }
}
