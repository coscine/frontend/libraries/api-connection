const axios = require('axios');

import apiConnectionBasic from '../basic/api-connection-basic';

function getResourceTypeApiUrl() {
  return (
    'https://' +
    apiConnectionBasic.getHostName() +
    '/coscine/api/Coscine.Api.Resources/ResourceType/'
  );
}

export class ResourceTypeApi {
  public static getResourceTypes(
    thenHandler: any = apiConnectionBasic.defaultThenHandler,
    catchHandler: any = apiConnectionBasic.defaultOnCatch
  ) {
    apiConnectionBasic.setHeader();
    return axios
      .get(getResourceTypeApiUrl() + 'types')
      .then(thenHandler)
      .catch(catchHandler);
  }
  public static getEnabledResourceTypes(
    thenHandler: any = apiConnectionBasic.defaultThenHandler,
    catchHandler: any = apiConnectionBasic.defaultOnCatch
  ) {
    apiConnectionBasic.setHeader();
    return axios
      .get(getResourceTypeApiUrl() + 'types/-/enabled')
      .then(thenHandler)
      .catch(catchHandler);
  }
  public static getResourceType(
    resourceTypeId: any,
    thenHandler: any = apiConnectionBasic.defaultThenHandler,
    catchHandler: any = apiConnectionBasic.defaultOnCatch
  ) {
    apiConnectionBasic.setHeader();
    return axios
      .get(getResourceTypeApiUrl() + 'types/' + resourceTypeId)
      .then(thenHandler)
      .catch(catchHandler);
  }
}
