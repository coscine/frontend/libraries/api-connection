const axios = require('axios');

import apiConnectionBasic from '../basic/api-connection-basic';

const authHeaderKey = 'Authorization';

function getTOSApiUrl() {
  return (
    'https://' + apiConnectionBasic.getHostName() + '/coscine/api/Coscine.Api.User/TOS/'
  );
}

function setHeader(jwtToken: string) {
  axios.defaults.headers.common[authHeaderKey] =
    'Bearer ' + jwtToken;
}

export class TOSApi {
  public static getCurrentTOSVersion(
    thenHandler: any = apiConnectionBasic.defaultThenHandler,
    catchHandler: any = apiConnectionBasic.defaultOnCatch
  ) {
    apiConnectionBasic.setHeader();
    return axios
      .get(getTOSApiUrl())
      .then(thenHandler)
      .catch(catchHandler);
  }
  public static acceptCurrentTOSVersion(
    jwtToken: string,
    thenHandler: any = apiConnectionBasic.defaultThenHandler,
    catchHandler: any = apiConnectionBasic.defaultOnCatch
  ) {
    setHeader(jwtToken);
    return axios
      .post(getTOSApiUrl())
      .then(thenHandler)
      .catch(catchHandler);
  }
}
