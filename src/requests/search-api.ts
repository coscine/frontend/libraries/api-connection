const axios = require('axios');

import apiConnectionBasic from '../basic/api-connection-basic';

function getSearchApiUrl() {
  return 'https://' + apiConnectionBasic.getHostName() + '/coscine/api/Coscine.Api.Project/Search/';
}

export class SearchApi {
  public static getSearchResults(
    query: string,
    thenHandler: any = apiConnectionBasic.defaultThenHandler,
    catchHandler: any = apiConnectionBasic.defaultOnCatch
  ) {
    apiConnectionBasic.setHeader();
    return axios
      .get(getSearchApiUrl() + 'all/' + encodeURIComponent(query))
      .then(thenHandler)
      .catch(catchHandler);
  }

  public static getSearchResultsNoFilter(
    thenHandler: any = apiConnectionBasic.defaultThenHandler,
    catchHandler: any = apiConnectionBasic.defaultOnCatch
  ) {
    apiConnectionBasic.setHeader();
    return axios
      .get(getSearchApiUrl() + 'allNoFilter/')
      .then(thenHandler)
      .catch(catchHandler);
  }

  public static getSearchResultsForProject(
    projectId: string,
    query: string,
    thenHandler: any = apiConnectionBasic.defaultThenHandler,
    catchHandler: any = apiConnectionBasic.defaultOnCatch
  ) {
    apiConnectionBasic.setHeader();
    return axios
      .get(getSearchApiUrl() + 'project/' + projectId + '/' + encodeURIComponent(query))
      .then(thenHandler)
      .catch(catchHandler);
  }

  public static getSearchResultsForProjectNoFilter(
    projectId: string,
    thenHandler: any = apiConnectionBasic.defaultThenHandler,
    catchHandler: any = apiConnectionBasic.defaultOnCatch
  ) {
    apiConnectionBasic.setHeader();
    return axios
      .get(getSearchApiUrl() + 'projectNoFilter/' + projectId)
      .then(thenHandler)
      .catch(catchHandler);
  }
}
