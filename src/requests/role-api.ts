const axios = require('axios');

import apiConnectionBasic from '../basic/api-connection-basic';

function getRoleApiUrl() {
  return 'https://' + apiConnectionBasic.getHostName() + '/coscine/api/Coscine.Api.Project/Role/';
}

export class RoleApi {
  public static getRoles(
    thenHandler: any = apiConnectionBasic.defaultThenHandler,
    catchHandler: any = apiConnectionBasic.defaultOnCatch
  ) {
    apiConnectionBasic.setHeader();
    return axios
      .get(getRoleApiUrl())
      .then(thenHandler)
      .catch(catchHandler);
  }
}
