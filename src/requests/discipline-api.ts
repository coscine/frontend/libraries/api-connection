const axios = require('axios');

import apiConnectionBasic from '../basic/api-connection-basic';

function getDisciplineApiUrl() {
  return (
    'https://' + apiConnectionBasic.getHostName() + '/coscine/api/Coscine.Api.Project/Discipline/'
  );
}

export class DisciplineApi {
  public static getDisciplines(
    thenHandler: any = apiConnectionBasic.defaultThenHandler,
    catchHandler: any = apiConnectionBasic.defaultOnCatch
  ) {
    apiConnectionBasic.setHeader();
    return axios
      .get(getDisciplineApiUrl())
      .then(thenHandler)
      .catch(catchHandler);
  }
}
