const axios = require('axios');

import apiConnectionBasic from '../basic/api-connection-basic';

function getAdminApiUrl() {
  return 'https://' + apiConnectionBasic.getHostName() + '/coscine/api/Coscine.Api.Admin/Admin/';
}

export class AdminApi {
  public static GetProject(
    projectString: String,
    thenHandler: any = apiConnectionBasic.defaultThenHandler,
    catchHandler: any = apiConnectionBasic.defaultOnCatch
  ) {
    apiConnectionBasic.setHeader();
    return axios
      .get(getAdminApiUrl() + projectString)
      .then(thenHandler)
      .catch(catchHandler);
  }
  public static UpdateQuota(
    body: any,
    thenHandler: any = apiConnectionBasic.defaultThenHandler,
    catchHandler: any = apiConnectionBasic.defaultOnCatch
  ) {
    apiConnectionBasic.setHeader();
    return axios
      .post(getAdminApiUrl(), body)
      .then(thenHandler)
      .catch(catchHandler);
  }
}
