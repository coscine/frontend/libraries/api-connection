const axios = require('axios');

const authHeaderKey = 'Authorization';
const clientCorrolationIdKey = 'X-Coscine-Logging-CorrelationId';
axios.interceptors.request.use((request: any) => {
  if ((typeof coscine !== "undefined") && (typeof coscine.loading !== "undefined") && (typeof coscine.loading.counter !== "undefined")) {
    coscine.loading.counter++;
  }
  return request;
}, (error: any) => {
  return Promise.reject(error);
});
axios.interceptors.response.use(function (response: any) {
  if ((typeof coscine !== "undefined") && (typeof coscine.loading !== "undefined") && (typeof coscine.loading.counter !== "undefined")) {
    coscine.loading.counter--;
  }
  return response;
}, function (error: any) {
  if ((typeof coscine !== "undefined") && (typeof coscine.loading !== "undefined") && (typeof coscine.loading.counter !== "undefined")) {
    coscine.loading.counter--;
  }
  return Promise.reject(error);
});
export default {
  getHostName() {
    let hostName = window.location.hostname;
    if (hostName.indexOf(':') !== -1) {
      if (hostName.indexOf('https://') !== -1) {
        hostName = hostName.replace('https://', '');
      }
      hostName = hostName.substr(0, hostName.indexOf(':'));
    }
    return hostName;
  },
  preparePath(path: string) {
    let resultString = '';
    if (path.charAt(0) === '/') {
      path = path.substring(1);
    }
    let array = path.split('/');
    for (let i = 0; i < array.length; i++) {
      resultString += encodeURIComponent(array[i]);
      if(i !== array.length - 1) {
        resultString += '/';
      }
    }
    return resultString;
  },
  setHeader() {
    axios.defaults.headers.common[authHeaderKey] =
      'Bearer ' + (coscine as any).authorization.bearer;
    axios.defaults.headers.common[clientCorrolationIdKey] = (coscine as any).clientcorrolation.id;
  },
  defaultOnCatch(error: any) {
    // TODO: Do something with the error
    // tslint:disable-next-line: no-console
    console.log(error);
  },
  defaultThenHandler(response: any) {
    return;
  },
};
