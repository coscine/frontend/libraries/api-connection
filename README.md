## Api-Connection library

**OUTDATED**: Use [@coscine/api-client](https://git.rwth-aachen.de/coscine/frontend/libraries/api-client).

This library provides the api-connection functionality for all CoScInE Typescript applications.
